const initialState = {
  userData: {
    nama: 'onotrak',
    email: 'onotrak@codemasters.id',
    phoneNumber: '081234567890',
    address:
      'Jl. Ring Road Barat No. 001, Banyuraden, Gamping, Sleman, Yogyakarta',
  },
};

const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'ADD_USER':
      return {
        ...state,
        userData: action.data,
      };
    default:
      return state;
  }
};

export default userReducer;
