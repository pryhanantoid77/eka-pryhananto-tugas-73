const initialState = {
  dataHome: [],
};

const homeReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'ADD_DATA':
      return {
        ...state,
        dataHome: action.data,
      };
    case 'UPDATE_DATA':
      var newData = [...state.dataHome];
      var findIndex = state.dataHome.findIndex(value => {
        return value.id === action.data.idStore;
      });
      newData[findIndex] = action.data;
      return {
        ...state,
        dataHome: newData,
      };

    default:
      return state;
  }
};

export default homeReducer;
