const initialState = {
  dataKeranjang: [],
};

const cartReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'ADD_KERANJANG':
      return {
        ...state,
        dataKeranjang: action.data,
      };
    case 'DELETE_KERANJANG':
      return {
        ...state,
        dataKeranjang: [],
      };
    default:
      return state;
  }
};

export default cartReducer;
