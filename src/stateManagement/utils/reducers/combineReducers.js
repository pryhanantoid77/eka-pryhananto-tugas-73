import {combineReducers} from 'redux';
import homeReducer from './homeReducer';
import cartReducer from './cartReducers';
import userReducer from './authReducers';
import transaksiReducer from './transactionReducers';

const rootReducer = combineReducers({
  home: homeReducer,
  cart: cartReducer,
  user: userReducer,
  transaksi: transaksiReducer,
});

export default rootReducer;
