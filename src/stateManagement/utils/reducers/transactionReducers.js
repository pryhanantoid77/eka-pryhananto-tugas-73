const initialState = {
  dataTransaksi: [],
};

const transaksiReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'ADD_TRANSAKSI':
      return {
        ...state,
        dataTransaksi: action.data,
      };
    default:
      return state;
  }
};

export default transaksiReducer;
