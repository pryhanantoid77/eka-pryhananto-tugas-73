import {
  View,
  Text,
  ScrollView,
  TextInput,
  TouchableOpacity,
  KeyboardAvoidingView,
} from 'react-native';
import React, {useEffect, useState} from 'react';
import Icon from 'react-native-vector-icons/dist/AntDesign';
import Time from 'react-native-vector-icons/dist/Ionicons';
import DateTime from '@react-native-community/datetimepicker';
import moment from 'moment';
import {useDispatch, useSelector} from 'react-redux';

const HomeReducers = ({navigation, route}) => {
  const [isOpen, setIsOpen] = useState('');
  const [showTime, setShowTime] = useState(false);
  const [showTimeOpen, setShowTimeOpen] = useState(false);
  const [timeOpen, setTimeOpen] = useState('');
  const [timeClose, setTimeClose] = useState('');
  console.log('showtimeopn', showTimeOpen);
  const [name, setName] = useState('');
  const [address, setAddress] = useState('');
  const [rating, setRating] = useState('');
  const [hargaMinimal, setHargaMinimal] = useState('');
  const [hargaMaxsimal, setHargaMaxsimal] = useState('');
  const [desskripsi, setDesskripsi] = useState('');
  const [gambar, setGambar] = useState('');
  const dispatch = useDispatch();
  const {dataHome} = useSelector(state => state.home);
  const [status, setStatus] = useState(true);

  const addData = () => {
    var dataStore = [...dataHome];

    const data = {
      idStore: moment(new Date()).format('YYYYMMDDHHmmssSSS'),
      address: address,
      availableTime: timeOpen + '-' + timeClose,
      description: desskripsi,
      favourite: false,
      isOpen: isOpen,
      minimumPrice: hargaMinimal,
      maximumPrice: hargaMaxsimal,
      ratings: rating,
      storeImage: gambar,
      storeName: name,
    };
    console.log('data :', data);
    dataStore.push(data);
    console.log('dataStore :', dataStore);
    dispatch({type: 'ADD_DATA', data: dataStore});
    navigation.navigate('Home');
  };

  return (
    <View style={{flex: 1, padding: 22}}>
      <Text style={{fontSize: 20, fontWeight: 'bold', color: 'black'}}>
        Add Store Data
      </Text>
      <ScrollView showsVerticalScrollIndicator={false}>
        <KeyboardAvoidingView
          behavior="padding"
          enabled
          keyboardVerticalOffset={-500}>
          <View style={{marginTop: 10, padding: 15}}>
            <Text style={{color: 'black', fontWeight: 'bold'}}>Nama Toko</Text>
            <TextInput
              placeholder="Masukan Nama Toko"
              value={name}
              onChangeText={text => setName(text)}
              style={{borderColor: 'black', borderWidth: 1, borderRadius: 5}}
            />
            <Text style={{color: 'black', fontWeight: 'bold', marginTop: 7}}>
              Alamat
            </Text>
            <TextInput
              placeholder="Masukan Alamat"
              value={address}
              onChangeText={text => setAddress(text)}
              style={{borderColor: 'black', borderWidth: 1, borderRadius: 5}}
            />
            <Text style={{color: 'black', fontWeight: 'bold', marginTop: 7}}>
              Status Buka Tutup
            </Text>
            <View style={{flexDirection: 'row', marginTop: 10}}>
              <TouchableOpacity onPress={() => setIsOpen('BUKA')}>
                <View style={{flexDirection: 'row'}}>
                  <View
                    style={{
                      width: 20,
                      height: 20,
                      borderRadius: 20,
                      borderWidth: 1,
                      borderColor: 'black',
                      marginRight: 10,
                      backgroundColor: isOpen == 'BUKA' ? 'grey' : 'white',
                      alignItems: 'center',
                      justifyContent: 'center',
                    }}>
                    {isOpen ? (
                      <Icon name="check" size={15} color="white" />
                    ) : null}
                  </View>
                  <Text>Buka</Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => setIsOpen('TUTUP')}>
                <View style={{flexDirection: 'row', marginLeft: '20%'}}>
                  <View
                    style={{
                      width: 20,
                      height: 20,
                      borderRadius: 20,
                      borderWidth: 1,
                      borderColor: 'black',
                      backgroundColor: isOpen == 'TUTUP' ? 'grey' : 'white',
                      marginRight: 10,
                      alignItems: 'center',
                      justifyContent: 'center',
                    }}>
                    {isOpen ? (
                      <Icon name="check" size={15} color="white" />
                    ) : null}
                  </View>
                  <Text>Tutup</Text>
                </View>
              </TouchableOpacity>
            </View>
            <View
              style={{
                flexDirection: 'row',
                marginTop: 10,
                justifyContent: 'space-between',
              }}>
              <View style={{width: '47%'}}>
                <Text style={{color: 'black', fontWeight: 'bold'}}>
                  Jam Buka
                </Text>
                <View
                  style={{
                    borderRadius: 5,
                    borderColor: 'black',
                    borderWidth: 1,
                    width: '100%',
                    height: 40,
                    justifyContent: 'center',
                    paddingHorizontal: 5,
                  }}>
                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                    }}>
                    <Text>{timeOpen ? timeOpen : 'Pilih Jam'}</Text>
                    {/* <Text>{timeOpen}</Text> */}
                    <TouchableOpacity onPress={() => setShowTimeOpen(true)}>
                      <Time name="time" size={22} />
                    </TouchableOpacity>
                    {showTimeOpen ? (
                      <DateTime
                        // modal
                        // open={showTimeOpen}
                        mode="time"
                        display="spinner"
                        value={new Date()}
                        onChange={async (event, selectedDate) => {
                          console.log('1234 event', event);
                          console.log('1234 date', selectedDate);
                          setShowTimeOpen(false);
                          setTimeOpen(moment(selectedDate).format('HH:mm'));
                        }}
                        is24Hour={true}
                      />
                    ) : null}
                  </View>
                </View>
              </View>
              <View style={{width: '47%'}}>
                <Text style={{color: 'black', fontWeight: 'bold'}}>
                  Jam Tutup
                </Text>
                <View
                  style={{
                    borderRadius: 5,
                    borderColor: 'black',
                    borderWidth: 1,
                    width: '100%',
                    height: 40,
                    justifyContent: 'center',
                    paddingHorizontal: 5,
                  }}>
                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                    }}>
                    <Text>{timeClose ? timeClose : 'Pilih Jam'}</Text>

                    <TouchableOpacity onPress={() => setShowTime(!showTime)}>
                      <Time name="time" size={22} />
                    </TouchableOpacity>
                    {showTime ? (
                      <DateTime
                        // modal
                        mode="time"
                        display="spinner"
                        value={new Date()}
                        onChange={async (event, selectedDate) => {
                          console.log('1234 event', event);
                          console.log('1234 date', selectedDate);
                          setShowTime(false);
                          setTimeClose(moment(selectedDate).format('HH:mm'));
                        }}
                        is24Hour={true}
                      />
                    ) : null}
                  </View>
                </View>
              </View>
            </View>
            <Text style={{color: 'black', fontWeight: 'bold', marginTop: 7}}>
              Jumlah Rating
            </Text>
            <TextInput
              placeholder="Masukan Jumlah Rating"
              value={rating}
              onChangeText={text => setRating(text)}
              style={{borderColor: 'black', borderWidth: 1, borderRadius: 5}}
            />
            <View
              style={{
                flexDirection: 'row',
                marginTop: 10,
                justifyContent: 'space-between',
              }}>
              <View style={{width: '47%'}}>
                <Text style={{color: 'black', fontWeight: 'bold'}}>
                  Harga Minimal
                </Text>
                <TextInput
                  value={hargaMinimal}
                  onChangeText={text => setHargaMinimal(text)}
                  style={{
                    borderRadius: 5,
                    borderColor: 'black',
                    borderWidth: 1,
                    width: '100%',
                    height: 40,
                    justifyContent: 'center',
                    paddingHorizontal: 5,
                  }}
                />
              </View>
              <View style={{width: '47%'}}>
                <Text style={{color: 'black', fontWeight: 'bold'}}>
                  Harga Maximal
                </Text>
                <TextInput
                  value={hargaMaxsimal}
                  onChangeText={text => setHargaMaxsimal(text)}
                  style={{
                    borderRadius: 5,
                    borderColor: 'black',
                    borderWidth: 1,
                    width: '100%',
                    height: 40,
                    justifyContent: 'center',
                    paddingHorizontal: 5,
                  }}
                />
              </View>
            </View>
            <Text style={{color: 'black', fontWeight: 'bold', marginTop: 7}}>
              Deskripsi
            </Text>
            <TextInput
              placeholder="Masukan Deskripsi"
              value={desskripsi}
              onChangeText={text => setDesskripsi(text)}
              style={{borderColor: 'black', borderWidth: 1, borderRadius: 5}}
            />
            <Text style={{color: 'black', fontWeight: 'bold', marginTop: 7}}>
              Gambar Toko
            </Text>
            <TextInput
              placeholder="Masukan Link Gambar Toko"
              value={gambar}
              onChangeText={text => setGambar(text)}
              style={{borderColor: 'black', borderWidth: 1, borderRadius: 5}}
            />
            <TouchableOpacity
              onPress={() => addData()}
              style={{
                backgroundColor: 'grey',
                marginTop: 30,
                alignItems: 'center',
                justifyContent: 'center',
                height: 40,
              }}>
              <Text style={{color: 'white', fontWeight: 'bold', fontSize: 20}}>
                Simpan
              </Text>
            </TouchableOpacity>
          </View>
        </KeyboardAvoidingView>
      </ScrollView>
    </View>
  );
};

export default HomeReducers;
