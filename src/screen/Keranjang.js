import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  Image,
  TouchableOpacity,
  FlatList,
} from 'react-native';
import React, {useEffect, useState} from 'react';
import {COLORS} from '../constant';
import Icon from 'react-native-vector-icons/dist/Ionicons';
import Iconn from 'react-native-vector-icons/dist/Feather';
import {KERANJANG} from '../constant/data';
import {useDispatch, useSelector} from 'react-redux';
import moment from 'moment/moment';

const Keranjang = ({navigation, route}) => {
  const [data, setData] = useState([]);
  const {dataKeranjang} = useSelector(state => state.cart);
  const dispatch = useDispatch();

  useEffect(() => {
    // console.log('183091ed', dataKeranjang);
    // console.log('log1', dataKeranjang);
    if (dataKeranjang.length == 0) {
      var keranjangData = [...dataKeranjang];
      const data = {
        idCart: moment(new Date()).format('YYYYMMDDHHmmssSSS'),
        color: 'black',
        imageProduct:
          'https://assets.bwbx.io/images/users/iqjWHBFdfxIU/i4jPhKEFw1NE/v0/1200x-1.jpg',
        merk: 'Nike',
        note: 'ganti sol sepatu karena bagian sebelah kiri rusak, dan sekalian yang sebelah kanan.',
        size: '21',
        service: 'Ganti Sol Sepatu',
      };
      keranjangData.push(data);
      dispatch({type: 'ADD_KERANJANG', data: keranjangData});
    }
    // console.log('data', keranjangData);
    // console.log('after add :', dataKeranjang);
  }, []);
  const SliderItem = ({item}) => {
    return (
      <View style={styles.card}>
        {console.log('item', item)}
        <Image
          source={{uri: item.imageProduct}}
          style={{marginLeft: 10, height: 100, width: 100}}
        />
        <View style={styles.text}>
          <Text style={{color: COLORS.black, fontWeight: 'bold'}}>
            {item.merk} - {item.color} - {item.size}
          </Text>
          <Text style={{marginTop: 10}}>{item.service}</Text>
          <Text style={{marginTop: 10}}>Note : {item.note}</Text>
        </View>
      </View>
    );
  };

  return (
    <View style={styles.container}>
      {/* {console.log('1234', dataKeranjang)} */}
      <View style={styles.header}>
        <Icon
          name="arrow-back"
          size={22}
          color="black"
          onPress={() => navigation.goBack()}
        />
        <Text
          style={{
            color: COLORS.black,
            marginLeft: 5,
            fontSize: 18,
            fontWeight: 'bold',
          }}>
          Keranjang
        </Text>
      </View>
      <View style={styles.body}>
        {/* {console.log('039213', dataKeranjang)} */}
        <ScrollView
          showsVerticalScrollIndicator={false}
          contentContainerStyle={{paddingBottom: 50}}>
          <FlatList
            data={dataKeranjang}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({item, index}) => <SliderItem item={item} />}
          />
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <TouchableOpacity
              style={{marginTop: 50, width: 130}}
              onPress={() => navigation.navigate('FormPemesanan')}>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                <Iconn name="plus-square" size={22} color={COLORS.primary} />
                <Text
                  style={{
                    color: COLORS.primary,
                    fontWeight: 'bold',
                    marginLeft: 5,
                  }}>
                  Tambah Barang
                </Text>
              </View>
            </TouchableOpacity>
          </View>
          <View
            style={{
              flex: 1,
              paddingHorizontal: 22,
              marginTop: 20,
              alignItems: 'center',
            }}>
            <TouchableOpacity
              style={styles.bottomconfirm}
              onPress={() =>
                navigation.navigate('Checkout', {
                  user: route.params.dataUser,
                  store: route.params.dataDetail,
                  dataKeranjang,
                })
              }>
              <Text style={styles.textbottom}>Selanjutnya</Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </View>
    </View>
  );
};

export default Keranjang;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLORS.silver,
  },
  header: {
    height: 56,
    width: '100%',
    backgroundColor: COLORS.white,
    alignItems: 'center',
    flexDirection: 'row',
    paddingHorizontal: 22,
    shadowColor: COLORS.black,
    shadowOffset: {
      height: 2,
      width: 0,
    },
    shadowOpacity: 0.25,
    elevation: 5,
  },
  body: {
    paddingHorizontal: 22,
    // paddingTop: 10,
  },
  card: {
    backgroundColor: COLORS.white,
    height: 135,
    width: '100%',
    flexDirection: 'row',
    marginTop: 10,
    alignItems: 'center',
    borderRadius: 10,
  },
  text: {
    marginLeft: 10,
  },
  bottomconfirm: {
    marginTop: 30,
    backgroundColor: COLORS.primary,
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 8,
    paddingVertical: 15,
    // bottom: 0,
    // position: 'absolute',
    marginBottom: 20,
  },
  textbottom: {
    color: COLORS.white,
    fontSize: 16,
    fontWeight: 'bold',
  },
});
