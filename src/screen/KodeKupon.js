import {
  FlatList,
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import React from 'react';
import {COLORS} from '../constant';
import {KUPON} from '../constant/data';

const KodeKupon = ({navigation}) => {
  const SliderItem = ({item}) => {
    return (
      <View
        style={{
          backgroundColor: COLORS.white,
          padding: 20,
          borderRadius: 16,
          marginTop: 5,
          // flexDirection: 'row',
          shadowColor: '#000',
          shadowOffset: {
            width: 0,
            height: 2,
          },
          shadowOpacity: 0.23,
          shadowRadius: 2.62,
          elevation: 3,
          marginBottom: 10,
        }}>
        <TouchableOpacity
          style={{flexDirection: 'row'}}
          onPress={() => navigation.navigate('Kode Kupon', item)}>
          <View
            style={{
              backgroundColor: '#FFDFE0',
              height: 80,
              width: 80,
              borderRadius: 50,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Image source={item.img} />
          </View>
          <View
            style={{
              justifyContent: 'center',
              marginLeft: 10,
              // backgroundColor: 'red',
              width: '100%',
            }}>
            <Text style={{color: 'black', fontWeight: 'bold', fontSize: 16}}>
              {item.title}
            </Text>
            <Text>{item.ex}</Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity
          style={{right: 15, bottom: 10, position: 'absolute'}}
          onPress={() => navigation.navigate('FormPemesanan', item)}>
          <Text style={{color: COLORS.darkblue, fontWeight: 'bold'}}>
            Pakai Kupon
          </Text>
        </TouchableOpacity>
      </View>
    );
  };

  return (
    <View style={styles.container}>
      <ScrollView
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{paddingBottom: 20}}>
        <View
          style={{
            backgroundColor: COLORS.white,
            paddingVertical: 50,
            justifyContent: 'center',
          }}>
          <TouchableOpacity
            style={{
              backgroundColor: COLORS.primary,
              height: 45,
              width: 112,
              borderRadius: 8,
              justifyContent: 'center',
              alignItems: 'center',
              right: 22,
              position: 'absolute',
            }}>
            <Text style={{fontWeight: 'bold', color: 'white'}}>Gunakan</Text>
          </TouchableOpacity>
        </View>
        <View
          style={{
            marginTop: 29,
            paddingHorizontal: 20,
          }}>
          <FlatList
            data={KUPON}
            renderItem={({item}) => <SliderItem item={item} />}
          />
        </View>
      </ScrollView>
    </View>
  );
};

export default KodeKupon;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLORS.silver,
  },
});
