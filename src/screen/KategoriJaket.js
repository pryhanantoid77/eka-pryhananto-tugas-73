import {FlatList, Image, StyleSheet, Text, View} from 'react-native';
import React from 'react';
import {JAKET} from '../constant/data';
import {COLORS} from '../constant';

const KategoriJaket = () => {
  const SliderItem = ({item}) => {
    return (
      <View
        style={{
          backgroundColor: 'white',
          height: 82,
          width: 162,
          padding: 20,
          alignItems: 'center',
          justifyContent: 'center',
          marginTop: 10,
          borderColor: COLORS.primary,
          borderWidth: 1,
          marginHorizontal: 5,
          borderRadius: 10,
        }}>
        <Image source={item.img} />
        <Text>{item.name}</Text>
      </View>
    );
  };
  return (
    <View>
      <Text>KategoriTas</Text>
      <FlatList
        data={JAKET}
        renderItem={({item}) => <SliderItem item={item} />}
        // horizontal
        pagingEnabled
        snapToAlignment="center"
        showsHorizontalScrollIndicator={false}
        numColumns={2}
        columnWrapperStyle={{flex: 1, justifyContent: 'space-around'}}
      />
    </View>
  );
};

export default KategoriJaket;

const styles = StyleSheet.create({});
