import {
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  View,
  Modal,
  Button,
  TouchableOpacity,
  Dimensions,
  FlatList,
} from 'react-native';
import React, {useEffect, useState} from 'react';
import Icon from 'react-native-vector-icons/dist/Ionicons';
import {COLORS, DATA} from '../constant';
import {lokasiAlamat} from '../constant/data';
import {useDispatch, useSelector} from 'react-redux';
import moment from 'moment/moment';
import {Rating} from '../Rating';

const Home = ({navigation, route}) => {
  const [showModal, setShowModal] = useState(false);
  const [data, setData] = useState([]);
  const dispatch = useDispatch();
  const {dataHome} = useSelector(state => state.home);
  const {userData} = useSelector(state => state.user);

  useEffect(() => {
    console.log('log', dataHome);
    if (dataHome.length == 0) {
      var homeData = [...dataHome];
      const data = {
        idStore: moment(new Date()).format('YYYYMMDDHHmmssSSS'),
        address:
          'Jl. Palagan Tentara pelajar, Kec. Mlati, Kab. Sleman, Yogyakarta',
        availableTime: '09:00 - 21.00',
        description:
          'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Massa gravida mattis arcu interdum lectus egestas scelerisque. Blandit porttitor diam viverra amet nulla sodales aliquet est. Donec enim turpis rhoncus quis integer. Ullamcorper morbi donec tristique condimentum ornare imperdiet facilisi pretium molestie.',
        favourite: false,
        isOpen: false,
        minimumPrice: '20000',
        maximumPrice: '80000',
        ratings: 4.0,
        storeImage:
          'https://img.freepik.com/free-vector/shop-with-open-sign_23-2148555388.jpg',
        storeName: 'Toko Sepatu Basket',
      };
      homeData.push(data);
      dispatch({type: 'ADD_DATA', data: homeData});
    }
  }, []);

  const addFavourite = (value, like) => {
    console.log('ok123 data bafore', dataHome);
    let dataPush = dataHome;
    dataPush = dataPush.map(item => {
      if (value.idStore == item.idStore) {
        return {
          ...item,
          favourite: like,
        };
      }
      return item;
    });
    console.log('ok123 data after', dataPush);
    // setData(dataPush);
  };

  const SliderItem = ({item, addFavourite}) => {
    return (
      <TouchableOpacity
        // style={styles.card}
        onPress={() =>
          navigation.navigate('Detail', {data: item, user: userData})
        }>
        <View style={styles.card}>
          <View
            style={{
              flexDirection: 'row',
              // width: '100%',
              height: 135,
              // backgroundColor: 'red',
            }}>
            {console.log('storeImage', item.storeImage)}
            <Image
              source={{uri: item.storeImage}}
              resizeMode="contain"
              style={{
                height: '100%',
                width: 100,
                borderRadius: 15,
                // width: 100,
                // resizeMode: 'contain',
              }}
            />
            <View
              style={{
                flex: 1,
                justifyContent: 'center',
                marginHorizontal: 15,
                // width: '100%',
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  // width: '100%',
                  // backgroundColor: 'black',
                }}>
                <View style={{flexDirection: 'row'}}>
                  {<Rating rating={item.ratings} colorOn="#FFC107" />}
                </View>
                <TouchableOpacity
                  style={{right: 0, position: 'absolute'}}
                  onPress={() => {
                    addFavourite(item, !item.favourite);
                  }}>
                  {item.favourite ? (
                    <Icon name="heart" size={20} color="red" />
                  ) : (
                    <Icon name="md-heart-outline" color="red" size={20} />
                  )}
                </TouchableOpacity>
              </View>
              <Text> {item.ratings}</Text>
              <Text
                style={{
                  fontWeight: 'bold',
                  fontSize: 16,
                  color: COLORS.black,
                  paddingTop: 5,
                }}>
                {item.storeName}
              </Text>
              <Text
                numberOfLines={1}
                ellipsizeMode="tail"
                style={{paddingTop: 5}}>
                {item.address}
              </Text>
              <View
                style={{
                  marginTop: 10,
                  backgroundColor: item.isOpen ? '#11A84E1F' : '#E64C3C33',
                  width: 58,
                  height: 21,
                  borderRadius: 20,
                  alignItems: 'center',
                }}>
                <Text
                  style={{
                    fontWeight: 'bold',
                    color: item.isOpen ? '#11A84E' : '#EA3D3D',
                  }}>
                  {item.isOpen ? 'BUKA' : 'TUTUP'}
                </Text>
              </View>
            </View>
          </View>
        </View>
      </TouchableOpacity>
    );
  };

  return (
    <View style={styles.container}>
      {console.log('data return', dataHome)}
      <ScrollView
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{paddingBottom: 10}}>
        <View style={{paddingHorizontal: 22, height: 290}}>
          <View style={styles.heading}>
            <Image
              style={{width: 45, height: 45, resizeMode: 'contain'}}
              source={require('../assets/image/fotoprofil.png')}
            />
            <Icon
              name="cart"
              size={25}
              color="black"
              style={styles.iconKeranjang}
              onPress={() => navigation.navigate('Keranjang')}
            />
          </View>
          <Text style={{color: COLORS.darkblue, fontSize: 15}}>
            Hello, Agil!
          </Text>
          <Text style={{color: COLORS.black, fontSize: 25, fontWeight: 'bold'}}>
            Ingin merawat dan perbaiki sepatumu? cari disini
          </Text>
          <View
            style={{
              flexDirection: 'row',
              marginTop: 20,
              justifyContent: 'space-between',
              // width: '100%',
            }}>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                backgroundColor: COLORS.silver,
                borderRadius: 10,
                // width: '100%',
              }}>
              <Icon name="ios-search" size={25} style={{marginRight: 20}} />
              <TextInput style={styles.search} />
            </View>
            <Modal
              animationType={'slide'}
              transparent={true}
              visible={showModal}
              onRequestClose={() => {
                console.log('Modal has been closed.');
              }}>
              {/*All views of Modal*/}
              {/*Animation can be slide, slide, none*/}
              <View style={styles.modal}>
                <Text>Filter Search</Text>
                <Button
                  title="OKE"
                  onPress={() => {
                    setShowModal(!showModal);
                  }}
                />
              </View>
            </Modal>
            <TouchableOpacity
              style={styles.filter}
              onPress={() => {
                setShowModal(!showModal);
              }}>
              <Icon name="filter" size={25} />
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.downpage}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              marginTop: 17,
            }}>
            <View style={styles.kategori}>
              <TouchableOpacity
                onPress={() => navigation.navigate('KategoriSepatu')}>
                <Image
                  source={require('../assets/image/Sepatu.png')}
                  style={{height: 45, width: 45}}
                />
                <Text style={{color: COLORS.primary, fontWeight: 'bold'}}>
                  Sepatu
                </Text>
              </TouchableOpacity>
            </View>
            <View style={styles.kategori}>
              <TouchableOpacity
                onPress={() => navigation.navigate('KategoriJaket')}>
                <Image
                  source={require('../assets/image/Jaket.png')}
                  style={{height: 45, width: 45}}
                />
                <Text style={{color: COLORS.primary, fontWeight: 'bold'}}>
                  Jaket
                </Text>
              </TouchableOpacity>
            </View>
            <View style={styles.kategori}>
              <TouchableOpacity
                onPress={() => navigation.navigate('KategoriTas')}>
                <Image
                  source={require('../assets/image/Tas.png')}
                  style={{height: 45, width: 45}}
                />
                <Text style={{color: COLORS.primary, fontWeight: 'bold'}}>
                  Tas
                </Text>
              </TouchableOpacity>
            </View>
          </View>
          <View style={styles.rekomen}>
            <Text
              style={{fontSize: 16, fontWeight: 'bold', color: COLORS.black}}>
              Rekomendasi Terdekat
            </Text>
            <Text
              style={{fontSize: 16, fontWeight: 'bold', color: COLORS.primary}}>
              View All
            </Text>
          </View>
          {/* <TouchableOpacity onPress={() => navigation.navigate('Detail')}> */}
          <FlatList
            data={dataHome}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({item, index}) => (
              <SliderItem item={item} addFavourite={addFavourite} />
            )}
            // horizontal
            pagingEnabled
            snapToAlignment="center"
            showsHorizontalScrollIndicator={false}
            style={{width: '100%', height: '100%'}}
          />
          {/* </TouchableOpacity> */}
        </View>
      </ScrollView>
      <TouchableOpacity onPress={() => navigation.navigate('Form Store')}>
        <View
          style={{
            backgroundColor: COLORS.primary,
            right: 10,
            bottom: 10,
            width: 50,
            height: 50,
            position: 'absolute',
            borderRadius: 30,
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <Icon name="add" size={22} color={COLORS.white} />
        </View>
      </TouchableOpacity>
    </View>
  );
};

export default Home;

const styles = StyleSheet.create({
  container: {
    backgroundColor: COLORS.white,
    flex: 1,
  },
  heading: {
    flexDirection: 'row',
    marginTop: 56,
    alignItems: 'center',
    // paddingHorizontal: 22,
  },
  iconKeranjang: {
    right: 0,
    position: 'absolute',
  },
  search: {
    width: '80%',
    backgroundColor: COLORS.silver,
    borderRadius: 10,
    // width: 275,
  },
  filter: {
    backgroundColor: COLORS.silver,
    width: 45,
    height: 45,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
  },
  modal: {
    width: '80%',
    height: 300,
    backgroundColor: COLORS.silver,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
    marginTop: 80,
    marginHorizontal: 40,
  },
  downpage: {
    backgroundColor: COLORS.silver,
    height: '100%',
    width: '100%',
    paddingHorizontal: 22,
  },
  kategori: {
    height: 95,
    width: 95,
    backgroundColor: COLORS.white,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 12,
  },
  rekomen: {
    marginTop: 27,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  card: {
    backgroundColor: COLORS.white,
    height: 133,
    borderRadius: 15,
    marginTop: 10,
    width: '100%',
  },
});
