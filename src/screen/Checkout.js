import {
  FlatList,
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import React, {useState} from 'react';
import {COLORS} from '../constant';
import {CheckBox} from 'react-native-elements';
import Icon from 'react-native-vector-icons/dist/Ionicons';
import {listPembayaran} from '../constant/data';
import {convertCurrency} from '../helper';

const Checkout = ({navigation, route}) => {
  const [isSelected, setSelection] = useState('');
  console.log('route params : ', route.params);
  const user = route.params.user;
  const store = route.params.store;
  const dataKeranjang = route.params.dataKeranjang;
  console.log('jumlah barang : ', dataKeranjang.length);
  console.log('barang : ', dataKeranjang);
  const harga = dataKeranjang.length * 50000;
  const totalHarga = harga + 3000;

  return (
    <View style={styles.container}>
      <ScrollView
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{paddingBottom: 20}}>
        <View style={styles.customer}>
          <Text>Data Customer</Text>
          <Text style={{color: COLORS.black, marginVertical: 5}}>
            {user.nama + ' (' + user.phoneNumber + ')'}
          </Text>
          <Text style={{color: COLORS.black, marginBottom: 2}}>
            {user.address}
          </Text>
          <Text style={{color: COLORS.black}}>{user.email}</Text>
        </View>
        <View style={styles.outlet}>
          <Text>alamat Outlet Tujuan</Text>
          <Text style={{color: COLORS.black, marginVertical: 5}}>
            {store.storeName}
          </Text>
          <Text style={{color: COLORS.black, marginBottom: 2}}>
            {store.address}
          </Text>
        </View>
        <View
          style={{backgroundColor: COLORS.white, marginTop: 10, padding: 20}}>
          <Text>Barang</Text>
          <FlatList
            data={dataKeranjang}
            keyExtractor={(item, index) => index.toString()}
            renderItem={(item, index) => (
              <View style={styles.barang}>
                <Image
                  source={{uri: item.item.imageProduct}}
                  style={{
                    marginLeft: 10,
                    height: 75,
                    width: 75,
                    borderRadius: 10,
                  }}
                />
                <View style={{marginLeft: 10}}>
                  <Text style={{color: COLORS.black, fontWeight: 'bold'}}>
                    {item.item.merk +
                      ' - ' +
                      item.item.color +
                      ' - ' +
                      item.item.size}
                  </Text>
                  <Text style={{marginTop: 10}}>{item.item.service}</Text>
                  <Text style={{marginTop: 10}}>
                    Note : {item?.item.note ? item.item.note : '-'}
                  </Text>
                </View>
              </View>
            )}
          />
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              marginTop: 10,
            }}>
            <Text style={{color: COLORS.black}}>
              {dataKeranjang.length} Pasang
            </Text>
            <Text style={{color: COLORS.black, fontWeight: 'bold'}}>
              {convertCurrency(harga, '@')}
            </Text>
          </View>
        </View>
        <View style={styles.rincian}>
          <Text>Rincian Pembayaran</Text>
          <View
            style={{
              flexDirection: 'row',
              //   justifyContent: 'space-between',
              //   backgroundColor: 'aqua',
              //   width: '100%',
            }}>
            <Text style={{color: COLORS.black, width: '25%'}}>Cuci Sepatu</Text>
            <Text style={{color: '#FFC107', width: '30%'}}>
              {dataKeranjang.length}x Pasang
            </Text>
            <Text
              style={{color: COLORS.black, width: '45%', textAlign: 'right'}}>
              {convertCurrency(harga, 'Rp ')}
            </Text>
          </View>
          <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <Text style={{color: COLORS.black}}>Biaya Antar</Text>
            <Text style={{color: COLORS.black}}>Rp 3.000</Text>
          </View>
          <View
            style={{backgroundColor: '#EDEDED', height: 1, marginVertical: 5}}
          />
          <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <Text style={{color: COLORS.black}}>Total</Text>
            <Text style={{color: COLORS.black}}>
              {convertCurrency(totalHarga, 'Rp ')}
            </Text>
          </View>
        </View>
        <View
          style={{marginTop: 10, backgroundColor: COLORS.white, padding: 20}}>
          <Text>Pilih Pembayaran</Text>
          <ScrollView horizontal showsHorizontalScrollIndicator={false}>
            <View style={{flexDirection: 'row'}}>
              <TouchableOpacity onPress={() => setSelection('Bank Transfer')}>
                <View
                  style={{
                    backgroundColor:
                      isSelected == 'Bank Transfer' ? '#03426229' : 'white',
                    height: 82,
                    width: 162,
                    padding: 20,
                    alignItems: 'center',
                    justifyContent: 'center',
                    marginTop: 10,
                    borderColor:
                      isSelected == 'Bank Transfer' ? '#034262' : '#E1E1E1',
                    borderWidth: 1,
                    marginHorizontal: 5,
                    borderRadius: 10,
                  }}>
                  {isSelected == 'Bank Transfer' ? (
                    <View style={{position: 'absolute', right: 0, top: 0}}>
                      <Icon
                        name="ios-checkmark-circle"
                        size={22}
                        color="#034262"
                      />
                    </View>
                  ) : null}
                  <Image source={require('../assets/image/ovo.png')} />
                  <Text>Bank Transfer</Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => setSelection('OVO')}>
                <View
                  style={{
                    backgroundColor:
                      isSelected == 'OVO' ? '#03426229' : 'white',
                    height: 82,
                    width: 162,
                    padding: 20,
                    alignItems: 'center',
                    justifyContent: 'center',
                    marginTop: 10,
                    borderColor: isSelected == 'OVO' ? '#034262' : '#E1E1E1',
                    borderWidth: 1,
                    marginHorizontal: 5,
                    borderRadius: 10,
                  }}>
                  {isSelected == 'OVO' ? (
                    <View style={{position: 'absolute', right: 0, top: 0}}>
                      <Icon
                        name="ios-checkmark-circle"
                        size={22}
                        color="#034262"
                      />
                    </View>
                  ) : null}
                  <Image source={require('../assets/image/ovo.png')} />
                  <Text>OVO</Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => setSelection('E-Payment')}>
                <View
                  style={{
                    backgroundColor:
                      isSelected == 'E-Payment' ? '#03426229' : 'white',
                    height: 82,
                    width: 162,
                    padding: 20,
                    alignItems: 'center',
                    justifyContent: 'center',
                    marginTop: 10,
                    borderColor:
                      isSelected == 'E-Payment' ? '#034262' : '#E1E1E1',
                    borderWidth: 1,
                    marginHorizontal: 5,
                    borderRadius: 10,
                  }}>
                  {isSelected == 'E-Payment' ? (
                    <View style={{position: 'absolute', right: 0, top: 0}}>
                      <Icon
                        name="ios-checkmark-circle"
                        size={22}
                        color="#034262"
                      />
                    </View>
                  ) : null}
                  <Image source={require('../assets/image/ovo.png')} />
                  <Text>E-Payment</Text>
                </View>
              </TouchableOpacity>
            </View>
          </ScrollView>
        </View>
        <View
          style={{
            marginTop: 10,
            backgroundColor: COLORS.white,
            paddingHorizontal: 22,
          }}>
          <TouchableOpacity
            style={styles.botton}
            onPress={() =>
              navigation.navigate('Summary', {user, store, dataKeranjang})
            }>
            <Text
              style={{color: COLORS.white, fontSize: 16, fontWeight: 'bold'}}>
              Pesan Sekarang
            </Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </View>
  );
};

export default Checkout;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLORS.silver,
  },
  customer: {
    backgroundColor: COLORS.white,
    padding: 20,
  },
  outlet: {
    backgroundColor: COLORS.white,
    marginTop: 10,
    padding: 20,
  },
  barang: {
    // height: 135,
    width: '100%',
    flexDirection: 'row',
    // backgroundColor: 'red',
    marginTop: 10,
    alignItems: 'center',
    marginLeft: -10,
    marginBottom: 10,
    // borderRadius: 10,
  },
  rincian: {
    marginTop: 10,
    backgroundColor: COLORS.white,
    padding: 20,
  },
  botton: {
    backgroundColor: COLORS.primary,
    justifyContent: 'center',
    alignItems: 'center',
    height: 55,
    width: '100%',
    borderRadius: 10,
    marginTop: 20,
  },
});
