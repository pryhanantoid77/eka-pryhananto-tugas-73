import {
  FlatList,
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import React from 'react';
import Icon from 'react-native-vector-icons/dist/Ionicons';
import {COLORS} from '../constant';
import {useDispatch, useSelector} from 'react-redux';
import moment from 'moment/moment';

const Summary = ({navigation, route}) => {
  console.log('log', route.params);
  const user = route.params.user;
  const store = route.params.store;
  const dataKeranjang = route.params.dataKeranjang;
  const dispatch = useDispatch();
  const {dataTransaksi} = useSelector(state => state.transaksi);

  const storeData = () => {
    var dataTrans = [...dataTransaksi];
    const data = {
      idTransaction: moment(new Date()).format('YYYYMMDDHHmmssSSS'),
      reservationCode: moment(new Date()).format('DDHHmmss'),
      date: moment(new Date()).format('DD MMMM YYYY'),
      customerData: user,
      storeData: store,
      cartData: dataKeranjang,
    };
    dataTrans.push(data);
    dispatch({type: 'ADD_TRANSAKSI', data: dataTrans});
    dispatch({type: 'DELETE_KERANJANG'});
    navigation.navigate('ConfirmReservasi', {item: data});
  };

  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <Icon
          name="arrow-back"
          size={22}
          color="black"
          onPress={() => navigation.goBack()}
        />
        <Text
          style={{
            color: COLORS.black,
            marginLeft: 5,
            fontSize: 18,
            fontWeight: 'bold',
          }}>
          Summary
        </Text>
      </View>
      <View style={styles.body}>
        <ScrollView
          showsVerticalScrollIndicator={false}
          contentContainerStyle={{paddingBottom: 50}}>
          <View style={styles.card}>
            <Text style={{fontSize: 18, marginBottom: 10}}>Data Customer</Text>
            <Text style={{fontSize: 18, color: COLORS.black}}>
              {user.nama + ' (' + user.phoneNumber + ')'}
            </Text>
            <Text style={{fontSize: 18, color: COLORS.black}}>
              {user.address}
            </Text>
            <Text style={{fontSize: 18, color: COLORS.black}}>
              {user.email}
            </Text>
          </View>
          <View style={styles.card}>
            <Text style={{fontSize: 18, marginBottom: 10}}>
              Alamat Outlet Tujuan
            </Text>
            <Text style={{fontSize: 18, color: COLORS.black}}>
              {store.storeName}
            </Text>
            <Text style={{fontSize: 18, color: COLORS.black}}>
              {store.address}
            </Text>
          </View>
          <View style={styles.card}>
            <Text style={{fontSize: 18, marginBottom: 10}}>Barang</Text>
            <FlatList
              data={dataKeranjang}
              keyExtractor={(item, index) => index.toString()}
              renderItem={(item, index) => (
                <View style={styles.barang}>
                  <Image
                    source={{uri: item.item.imageProduct}}
                    style={{
                      marginLeft: 10,
                      height: 75,
                      width: 75,
                      borderRadius: 10,
                    }}
                  />
                  <View style={{marginLeft: 10}}>
                    <Text style={{color: COLORS.black, fontWeight: 'bold'}}>
                      {item.item.merk +
                        ' - ' +
                        item.item.color +
                        ' - ' +
                        item.item.size}
                    </Text>
                    <Text style={{marginTop: 10}}>{item.item.service}</Text>
                    <Text style={{marginTop: 10}}>
                      Note : {item?.item.note ? item.item.note : '-'}
                    </Text>
                  </View>
                </View>
              )}
            />
          </View>
          <View style={{flex: 1, paddingHorizontal: 22, alignItems: 'center'}}>
            <TouchableOpacity
              style={styles.bottomconfirm}
              onPress={() => storeData()}>
              <Text style={styles.textbottom}>Reservasi Sekarang</Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </View>
    </View>
  );
};

export default Summary;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLORS.silver,
  },
  header: {
    height: 56,
    width: '100%',
    backgroundColor: COLORS.white,
    alignItems: 'center',
    flexDirection: 'row',
    paddingHorizontal: 22,
    shadowColor: COLORS.black,
    shadowOffset: {
      height: 2,
      width: 0,
    },
    shadowOpacity: 0.25,
    elevation: 5,
  },
  body: {},
  card: {
    backgroundColor: COLORS.white,
    // height: 175,
    paddingVertical: 22,
    justifyContent: 'center',
    width: '100%',
    marginTop: 10,
    paddingHorizontal: 22,
  },
  bottomconfirm: {
    marginTop: 30,
    backgroundColor: COLORS.primary,
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 8,
    paddingVertical: 15,
    // bottom: 0,
    // position: 'absolute',
    marginBottom: 20,
  },
  textbottom: {
    color: COLORS.white,
    fontSize: 16,
    fontWeight: 'bold',
  },
  barang: {
    // height: 135,
    width: '100%',
    flexDirection: 'row',
    // backgroundColor: 'red',
    marginTop: 10,
    alignItems: 'center',
    marginLeft: -10,
    marginBottom: 10,
    // borderRadius: 10,
  },
});
