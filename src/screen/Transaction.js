import {
  FlatList,
  Modal,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import React, {useState} from 'react';
import {COLORS} from '../constant';
import Icon from 'react-native-vector-icons/dist/Ionicons';
import {useDispatch, useSelector} from 'react-redux';

const Transaction = ({navigation}) => {
  const {dataTransaksi} = useSelector(state => state.transaksi);
  console.log('data', dataTransaksi);
  // console.log('cardData :', dataTransaksi[0].cartData[0]);

  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <Icon
          name="arrow-back"
          size={22}
          color="black"
          onPress={() => navigation.goBack()}
        />
        <Text
          style={{
            color: COLORS.black,
            marginLeft: 5,
            fontSize: 18,
            fontWeight: 'bold',
          }}>
          Transaction
        </Text>
      </View>
      {/* <ScrollView
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{paddingBottom: 20}}> */}
      <View style={styles.body}>
        <FlatList
          showsVerticalScrollIndicator={false}
          contentContainerStyle={{paddingBottom: 60}}
          data={dataTransaksi}
          keyExtractor={(item, index) => index.toString()}
          renderItem={(item, index) => (
            // console.log('data item : ', item)
            <View style={styles.card}>
              <TouchableOpacity
                onPress={() => navigation.navigate('KodeReservasi', item)}>
                <Text>20 Desember 2020</Text>
                {/* {console.log('data item : ', item.item.cartData)} */}
                <Text
                  style={{
                    fontWeight: '500',
                    color: COLORS.black,
                    marginTop: 13,
                  }}>
                  {item.item.cartData[0].merk +
                    ' - ' +
                    item.item.cartData[0].color +
                    ' - ' +
                    item.item.cartData[0].size}
                </Text>
                <Text style={{color: COLORS.black}}>
                  {item.item.cartData[0].service}
                </Text>
                <View style={{flexDirection: 'row', marginTop: 13}}>
                  <Text style={{color: COLORS.black}}>Kode Reservasi : </Text>
                  <Text style={{color: COLORS.black, fontWeight: 'bold'}}>
                    {item.item.reservationCode}
                  </Text>
                  <View style={styles.status}>
                    <Text style={{color: '#FFC107'}}>Reserved</Text>
                  </View>
                </View>
              </TouchableOpacity>
            </View>
          )}
        />
      </View>
      {/* </ScrollView> */}
      {/* <TouchableOpacity onPress={() => setShowModal(!showModal)}>
        <View
          style={{
            backgroundColor: COLORS.primary,
            right: 10,
            bottom: 10,
            width: 50,
            height: 50,
            position: 'absolute',
            borderRadius: 30,
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <Icon name="plus" size={22} color={COLORS.white} />
        </View>
      </TouchableOpacity> */}
    </View>
  );
};

export default Transaction;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLORS.silver,
  },
  header: {
    height: 56,
    width: '100%',
    backgroundColor: COLORS.white,
    alignItems: 'center',
    flexDirection: 'row',
    paddingHorizontal: 22,
    shadowColor: COLORS.black,
    shadowOffset: {
      height: 2,
      width: 0,
    },
    shadowOpacity: 0.25,
    elevation: 5,
  },
  body: {
    paddingHorizontal: 15,
  },
  card: {
    marginTop: 10,
    backgroundColor: COLORS.white,
    paddingHorizontal: 10,
    paddingVertical: 15,
    borderRadius: 10,
  },
  status: {
    right: 0,
    position: 'absolute',
    backgroundColor: '#F29C1F29',
    borderRadius: 10,
    paddingHorizontal: 10,
  },
});
