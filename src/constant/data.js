export const lokasiAlamat = [
  {
    id: 1,
    img: require('../assets/image/gejayan.png'),
    title: 'Jack Repair Gejayan',
    rating: '4.8',
    address: 'Jl. Gejayan III no.2, Karangasem, Kec.Laweyan',
    status: 'TUTUP',
    love: false,
  },
  {
    id: 2,
    img: require('../assets/image/seturan.png'),
    title: 'Jack Repair Seturan',
    rating: '4.8',
    address: 'Jl. Seturan, Kec.Laweyan',
    status: 'BUKA',
    love: true,
  },
  {
    id: 3,
    img: require('../assets/image/seturan.png'),
    title: 'Jack Repair Seturan',
    rating: '4.8',
    address: 'Jl. Seturan, Kec.Laweyan',
    status: 'BUKA',
    love: true,
  },
  {
    id: 4,
    img: require('../assets/image/seturan.png'),
    title: 'Jack Repair Seturan',
    rating: '4.8',
    address: 'Jl. Seturan, Kec.Laweyan',
    status: 'BUKA',
    love: true,
  },
];

export const listPembayaran = [
  {
    id: 1,
    img: require('../assets/image/ovo.png'),
    title: 'Bank Transfer',
  },
  {
    id: 2,
    img: require('../assets/image/ovo.png'),
    title: 'OVO',
  },
  {
    id: 3,
    img: require('../assets/image/ovo.png'),
    title: 'E-Payment',
  },
];

export const SEPATU = [
  {
    id: 1,
    img: require('../assets/image/Sepatu.png'),
    name: 'Sepatu',
  },
  {
    id: 2,
    img: require('../assets/image/Sepatu.png'),
    name: 'Sepatu',
  },
  {
    id: 3,
    img: require('../assets/image/Sepatu.png'),
    name: 'Sepatu',
  },
  {
    id: 4,
    img: require('../assets/image/Sepatu.png'),
    name: 'Sepatu',
  },
  {
    id: 5,
    img: require('../assets/image/Sepatu.png'),
    name: 'Sepatu',
  },
];
export const TAS = [
  {
    id: 1,
    img: require('../assets/image/Tas.png'),
    name: 'Tas',
  },
  {
    id: 2,
    img: require('../assets/image/Tas.png'),
    name: 'Tas',
  },
  {
    id: 3,
    img: require('../assets/image/Tas.png'),
    name: 'Tas',
  },
  {
    id: 4,
    img: require('../assets/image/Tas.png'),
    name: 'Tas',
  },
  {
    id: 5,
    img: require('../assets/image/Tas.png'),
    name: 'Tas',
  },
];
export const JAKET = [
  {
    id: 1,
    img: require('../assets/image/Jaket.png'),
    name: 'Jaket',
  },
  {
    id: 2,
    img: require('../assets/image/Jaket.png'),
    name: 'Jaket',
  },
  {
    id: 3,
    img: require('../assets/image/Jaket.png'),
    name: 'Jaket',
  },
  {
    id: 4,
    img: require('../assets/image/Jaket.png'),
    name: 'Jaket',
  },
  {
    id: 5,
    img: require('../assets/image/Jaket.png'),
    name: 'Jaket',
  },
];

export const KERANJANG = [
  {
    id: 1,
    img: 'https://asset-a.grid.id/crop/0x0:0x0/700x0/photo/2021/03/05/img_20210305_012956jpg-20210305024434.jpg',
    merk: 'New Balance',
    color: 'Pink Abu',
    size: '40',
    checkbox: 'Cuci Sepatu',
    note: '-',
  },
];

export const KUPON = [
  {
    id: 1,
    img: require('../assets/image/30rb.png'),
    title: 'Promo Cashback Hingga 30rb',
    ex: '09 s/d 15 Maret 2021',
  },
  {
    id: 2,
    img: require('../assets/image/30rb.png'),
    title: 'Promo Cashback Hingga 40rb',
    ex: '09 s/d 15 Maret 2021',
  },
  {
    id: 3,
    img: require('../assets/image/30rb.png'),
    title: 'Promo Cashback Hingga 50rb',
    ex: '09 s/d 15 Maret 2021',
  },
  {
    id: 4,
    img: require('../assets/image/30rb.png'),
    title: 'Promo Cashback Hingga 60rb',
    ex: '09 s/d 15 Maret 2021',
  },
];
